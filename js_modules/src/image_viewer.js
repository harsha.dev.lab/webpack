// gets saved to build directory
// import big from "../assets/big.jpg";

// gets base64 encoded into build.js
import small from "../assets/small.jpg";
import "../styles/image_viewer.css";

// const image = document.createElement("img");

// // lorempixel no longer works
// // image.src = "http://lorempixel.com/400/400";

// image.src = small;
// document.body.appendChild(image);

// const bigImage = document.createElement("img");
// bigImage.src = big;

// document.body.appendChild(bigImage);

export default () => {
  const image = document.createElement("img");
  image.src = small;
  document.body.appendChild(image);
}
