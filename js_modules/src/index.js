// calls functions from sum.js

// require without babel
// const sum = require("./sum");

// import with babel
// import sum from "./sum";

// nothing is exported, file just runs
// import "./image_viewer";

// const total = sum(10, 5);
// console.log(total);

const button = document.createElement("button");
button.innerText = "Click Me";

// load image_viewer.js only when button is clicked
// also loads any imports inside image_viewer
// this is called code splitting
button.onclick = () => {
  System.import("./image_viewer").then(module => {
    // console.log(module);
    module.default();
  });
};

document.body.appendChild(button);
