// utility functions for working with mathmatical calculations

const sum = (a, b) => a + b;

// export without babel
// module.exports = sum;

// export with babel
export default sum;
