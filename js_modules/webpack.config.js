const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const config = {
  entry: "./src/index.js", // webpack entrypoint
  output: {
    path: path.resolve(__dirname, "build"), // must be absolute filepath
    filename: "bundle.js", // filename is bundle.js by convention
    publicPath: "build/"
  },
  // in webpack1, this was called loader
  // in webpack2, this became module
  module: {
    // define separate loaders or rules
    rules: [
      {
        use: "babel-loader",
        test: /\.js$/
      },
      {
        // use: [
        //   "style-loader",
        //   "css-loader"
        // ],
        // extract-text-webpack-plugin requires loader instead of use
        loader: ExtractTextPlugin.extract({
          loader: "css-loader"
        }),
        test: /\.css$/
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        use: [
          {
            loader: "url-loader",
            // if less than 40kB, load image
            // otherwise save image
            options: {
              limit: 40000
            }
          },
          "image-webpack-loader"
        ]
      }
    ]
  },
  plugins: [
    // creates style.css and stops css from ending up in bundle.js
    new ExtractTextPlugin("style.css")
  ]
};

module.exports = config;
